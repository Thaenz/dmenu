/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = { 
	"monospace:size=10"
};
static const char *prompt = NULL;      /* -p  option; prompt to the left of input field */
static int colorprompt    = 1;         /* -p  option; if 1, prompt uses SchemeSel, otherwise SchemeNorm */
static const char *colors[SchemeLast][2] = {
	/*               fg         bg       */
	[SchemeNorm] = { "#AAAAAA", "#000000" },
	[SchemeSel]  = { "#000000", "#AAAAAA" },
	[SchemeOut]  = { "#AAAAAA", "#000000" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 20;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static unsigned int border_width = 1;
